package com.senac.controlecombustivel;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.senac.controlecombustivel.banco.BackupLog;
import com.senac.controlecombustivel.banco.BackupSQLiteHelper;
import com.senac.controlecombustivel.banco.BandeiraDAO;
import com.senac.controlecombustivel.banco.CombustivelDAO;
import com.senac.controlecombustivel.banco.PostoDAO;
import com.senac.controlecombustivel.banco.TipoDAO;
import com.senac.controlecombustivel.model.Bandeira;
import com.senac.controlecombustivel.model.Combustivel;
import com.senac.controlecombustivel.model.GPSTracker;
import com.senac.controlecombustivel.model.Posto;
import com.senac.controlecombustivel.model.Tipo;
import com.senac.controlecombustivel.webservice.WebService;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class EncontrarPostosActivity extends AppCompatActivity {

    private GoogleMap mapa; // Pode ser nulo, se o Google Play services APK nao estiver disponivel

    private List<Marker> marcacoes;

    private double latitude;
    private double longitude;

    private List<Posto> postos;

    private LatLng posicao;
    private Circle circulo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encontrar_postos);

        Log.i("Track", EncontrarPostosActivity.class + " OnCreate");

        inicializarMapa();

        Tracker tracker = CGCApp.tracker();
        tracker.setScreenName("Encontrar Postos Activity");
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    private void inicializarMapa() {
        Log.i("Track", EncontrarPostosActivity.class + " InicializarMapa");
        GPSTracker gps = new GPSTracker(this);
        gps = new GPSTracker(EncontrarPostosActivity.this);

        // Verifica se o GPS está habilitado.
        Log.i("Track", EncontrarPostosActivity.class + " Verifica Se GPS Está Habilitado");
        if (gps.canGetLocation()) {
            Log.i("Track", EncontrarPostosActivity.class + " GPS Está Habilitado");
            // Pegando a latitude e longitude do GPS.
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();

            posicao = new LatLng(latitude, longitude);

            criarMapaSeNecessario();

            // Cria as marcacoes com os dados dos postos, e adiciona no mapa.
            addMarcacoesPostos();
        } else {
            Log.i("Track", EncontrarPostosActivity.class + " GPS Não Está Habilitado");
            // Can't get location.
            // GPS or network is not enabled.
            // Ask user to enable GPS/network in settings.
            gps.showSettingsAlert();
        }

        mapa.moveCamera(CameraUpdateFactory.newLatLng(posicao));
    }

    private void addMarcacoesPostos() {
        // Se os postos não for nulo, limpa a lista.
        if (postos != null)
            postos = null;

        postos = WebService.getPostos();

        for (Posto p : postos) {
            if (calcularDistancia(latitude, longitude, p.getLatitude(), p.getLongitude()) <= 1000) {
                MarkerOptions markerOptions = new MarkerOptions()
                        .position(new LatLng(p.getLatitude(), p.getLongitude()))
                        .title(p.getNome())
                        .visible(true)
                        .anchor(0.5f, 0.5f)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_local_gas_station_black_48dp));
                marcacoes.add(mapa.addMarker(markerOptions));
            } else {
                MarkerOptions markerOptions = new MarkerOptions()
                        .position(new LatLng(p.getLatitude(), p.getLongitude()))
                        .title(p.getNome())
                        .visible(false)
                        .anchor(0.5f, 0.5f)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_local_gas_station_black_48dp));
                marcacoes.add(mapa.addMarker(markerOptions));
            }
        }
        Log.d("ADD POSTOS", marcacoes.size() + " | " + postos.size());
    }

    private void criarMapaSeNecessario() {
        Log.i("Track", EncontrarPostosActivity.class + " Criar Mapa Se Necessario");
        // Verifica se o mapa, ainda não foi instanciado.
        if (mapa == null) {
            // Tenta obter o mapa do SupportMapFragment.
            mapa = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapa))
                    .getMap();

            Log.i("Tracker", EncontrarPostosActivity.class + " Mapa " + mapa.toString());
            // Verifica se a obtenção do mapa, foi bem sucedida.
            if (mapa != null) {
                Log.i("Tracker", EncontrarPostosActivity.class + " Mapa Não É Nulo");
                criarMapa();
            } else {
                Log.i("Tracker", EncontrarPostosActivity.class + " Mapa É Nulo");
            }
        }
    }



    private void criarMapa() {
        marcacoes = new ArrayList<>();

        // Adicionando o circulo com raio de 1000 metros.
        circulo = mapa.addCircle(new CircleOptions()
                .center(posicao)
                .radius(1000)
                .fillColor(Color.argb(100, 255, 169, 31))
                .strokeWidth(1)
                .strokeColor(Color.argb(255, 255, 169, 31)));

        // Movendo a camera para a localizacao, atual, do usuario.
        mapa.moveCamera(CameraUpdateFactory.newLatLng(posicao));

        // Criando a marcacao da localizacao, atual, do usuario.
        MarkerOptions markerOptions = new MarkerOptions()
                .anchor(0.5f, 0.5f)
                .position(posicao)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_my_location_black_48dp));

        // Adicionando a marcacao no mapa e na lista de marcacoes.
        marcacoes.add(mapa.addMarker(markerOptions));

        // Definindo o tratador de clicks nas marcacoes
        mapa.setOnMarkerClickListener(new MarcadorListener());
    }

    /**
     * Calcula a distancia, em metros, de um ponto geografico, para outro ponto geografico,
     * recebendo a latitude e longitude, de cada ponto.
     *
     * @param deLat    Latitude do ponte inicial.
     * @param deLong   Longitude do ponto inicial.
     * @param paraLat  Latitude do ponto final.
     * @param paraLong Longitude do ponto final.
     * @return Distancia, em metros, entre os dois pontos.
     */
    private double calcularDistancia(double deLat, double deLong,
                                     double paraLat, double paraLong) {
        double d2r = Math.PI / 180;
        double dLong = (paraLong - deLong) * d2r;
        double dLat = (paraLat - deLat) * d2r;
        double a = Math.pow(Math.sin(dLat / 2.0), 2) + Math.cos(deLat * d2r)
                * Math.cos(paraLat * d2r) * Math.pow(Math.sin(dLong / 2.0), 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = 6367000 * c;
        return Math.round(d);
    }


    private class MarcadorListener implements GoogleMap.OnMarkerClickListener {

        @Override
        public boolean onMarkerClick(Marker marker) {
            if (marker.isVisible() && !marker.getId().equalsIgnoreCase("m0")) {
                Intent intent = new Intent(EncontrarPostosActivity.this, InformacoesPostoActivity.class);
                // Inserindo o id do posto pra proxima tela e substituindo o 'm' do nome da marcacao por nada
                intent.putExtra("posto", postos.get(Integer.parseInt(marker.getId().replaceAll("m", "")) - 1));

                // Google Analytics Event Handler
                HitBuilders.EventBuilder eventBuilder = new HitBuilders.EventBuilder();

                String label = "Informações do Posto " +
                        postos.get(Integer.parseInt(marker.getId().replaceAll("m", "")) - 1).getNome();

                eventBuilder.setAction("Clique")
                        .setLabel(label)
                        .setCategory("Marcação");

                CGCApp.tracker().send(eventBuilder.build());

                startActivity(intent);
            } else {
                Log.e("EVENTO MARCAÇÃO", "TENTATIVA DE VISUALIZAR A MARCACAO ZERO");
            }
            return false;
        }
    }

    // Trata o evento nos cliques dos botões da tela.

    public void atualizarLocalizacao(View view) {
        GPSTracker gps = new GPSTracker(this);

        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();

            posicao = new LatLng(latitude, longitude);

            // Google Analytics Event Handler
            HitBuilders.EventBuilder eventBuilder = new HitBuilders.EventBuilder();

            String label = "Atualizando Mapa em LagLng(" + latitude + "," + longitude + ")";

            eventBuilder.setAction("Clique")
                    .setLabel(label)
                    .setCategory("Botão");

            CGCApp.tracker().send(eventBuilder.build());

            atualizarMapa();
        } else {
            // Can't get location.
            // GPS or network is not enabled.
            // Ask user to enable GPS/network in settings.
            gps.showSettingsAlert();
        }
    }

    private void atualizarMapa() {
        // Mudar a localização do circulo
        circulo.setCenter(posicao);

        // Atualizando a posição da marcação do usuário.
        Marker localizacaoUsuario = marcacoes.get(0);
        localizacaoUsuario.setPosition(posicao);

        // Deixar as marcações no raio de 1000 metros visiveis
        for (Marker marker : marcacoes) {
            if (calcularDistancia(latitude, longitude,
                    marker.getPosition().latitude,
                    marker.getPosition().longitude) <= 1000) {
                // Deixa a marcação visivel no mapa.
                marker.setVisible(true);
            } else {
                marker.setVisible(false);
            }
        }

        mapa.moveCamera(CameraUpdateFactory.newLatLng(posicao));
    }

    public void mostrarRelatorios(View view) {
        Intent intent = new Intent(this, VisualizarRelatorioActivity.class);
        startActivity(intent);

        // Google Analytics Event Handler
        HitBuilders.EventBuilder eventBuilder = new HitBuilders.EventBuilder();

        String label = "Visualizar Relatório EncontrarPostosActivity";

        eventBuilder.setAction("Clique")
                .setLabel(label)
                .setCategory("Botão");

        CGCApp.tracker().send(eventBuilder.build());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_action_bar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.menu_relatorio:
                startActivity(new Intent(this, VisualizarRelatorioActivity.class));
                // Google Analytics Event Handler
                HitBuilders.EventBuilder eventBuilder = new HitBuilders.EventBuilder();

                String label = "Visualizar Relatório EncontrarPostosActivity";

                eventBuilder.setAction("Clique")
                        .setLabel(label)
                        .setCategory("Barra de Ação");

                CGCApp.tracker().send(eventBuilder.build());
                return true;
            case R.id.menu_feedback:
                ControladorFeedback controladorFeedback = new ControladorFeedback(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    // Trata do backup local, feito no SQLite.

    private void fazBackupSeNecessario() {
        BackupLog backupLog = new BackupLog(this);

        Calendar dataUltimoBackup = backupLog.getUltimaData();

        Calendar agora = Calendar.getInstance();

        // Se a data do ultimo backup NÃO for nula
        // verifica se ela é mais é anterior a data atual,
        // se for, realiza o backup.
        if (dataUltimoBackup != null) {

            if (dataBackupAnteriorAtual(dataUltimoBackup)) {
                realizaBackup();
                // Insere na tabela BACKUP LOG a ultima data de backup.
                backupLog.inserirBackupLog(agora);
            }
            // Se a data do ultimo backup retornar nula,
            // realiza o backup, e insere a data de ultimo backup.
        } else {
            realizaBackup();
            // Insere na tabela BACKUP LOG a ultima data de backup.
            backupLog.inserirBackupLog(agora);
        }
    }

    private boolean dataBackupAnteriorAtual(Calendar dataUltimoBackup) {
        Calendar atual = Calendar.getInstance();

        if (atual.get(Calendar.YEAR) > dataUltimoBackup.get(Calendar.YEAR))
            return true;

        if (atual.get(Calendar.MONTH) > dataUltimoBackup.get(Calendar.MONTH))
            return true;

        return atual.get(Calendar.DAY_OF_MONTH) > dataUltimoBackup.get(Calendar.DAY_OF_MONTH);
    }

    private void realizaBackup() {
        // Deleta os valores nas tabelas
        new BackupSQLiteHelper(this).truncateDatabase();
        // Insere o backup nas tabelas
        backupBandeiras(WebService.getBackupBandeiras());
        backupTipos(WebService.getBackupTipos());
        backupCombustiveis(WebService.getBackupCombustiveis());
        backupPostos(WebService.getBackupPostos());
    }

    private void backupPostos(List<Posto> backupPostos) {
        PostoDAO banco = new PostoDAO(this);

        for (Posto p : backupPostos) {
            banco.inserirPosto(p);
        }
    }

    private void backupCombustiveis(List<Combustivel> backupCombustiveis) {
        CombustivelDAO banco = new CombustivelDAO(this);

        for (Combustivel c : backupCombustiveis) {
            banco.inserirCombustivel(c);
        }
    }

    private void backupTipos(List<Tipo> backupTipos) {
        TipoDAO banco = new TipoDAO(this);

        for (Tipo t : backupTipos) {
            banco.inserirTipo(t);
        }
    }

    private void backupBandeiras(List<Bandeira> backupBandeiras) {
        BandeiraDAO banco = new BandeiraDAO(this);

        for (Bandeira b : backupBandeiras) {
            banco.inserirBandeira(b);
        }

        banco.getBandeiras();
    }

}